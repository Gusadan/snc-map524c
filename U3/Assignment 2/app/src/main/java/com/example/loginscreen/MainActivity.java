package com.example.loginscreen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.QuickContactBadge;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button loginButton;
    LoginManager loginManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginManager = new LoginManager();

        loginButton = findViewById(R.id.loginButton);
        loginButton.setOnClickListener(onClickLoginButton);
    }

    private View.OnClickListener onClickLoginButton = new View.OnClickListener(){

        @Override
        public void onClick(View view) {
            EditText userNameEditText = findViewById(R.id.usernameEditText);
            EditText passwordEditText = findViewById(R.id.passwordEditText);
            TextView errorTextView = findViewById(R.id.ErrorTextView);

            String userName = userNameEditText.getText().toString();
            String password = passwordEditText.getText().toString();

            if(loginManager.getLoginAttemptsLeft() > 1){
                if(loginManager.hasValidCredentials(userName, password)) {
                    errorTextView.setText("successfully logged in");
                    loginManager.setLoginAttemptsLeft(3);
                }else{
                    loginManager.setLoginAttemptsLeft(loginManager.getLoginAttemptsLeft() - 1);
                    errorTextView.setText(getText(R.string.error_text) + " " + loginManager.getLoginAttemptsLeft() + " login attempts left");
                    errorTextView.setVisibility(View.VISIBLE);
                }
            }else {
                errorTextView.setText("Login locked");
                loginButton.setEnabled(false);
            }
        }
    };
}
