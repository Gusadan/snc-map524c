package com.example.loginscreen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.QuickContactBadge;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button loginButton = findViewById(R.id.loginButton);

        loginButton.setOnClickListener(onClickLoginButton);
    }

    private View.OnClickListener onClickLoginButton = new View.OnClickListener(){

        @Override
        public void onClick(View view) {
            EditText userNameEditText = findViewById(R.id.usernameEditText);
            EditText passwordEditText = findViewById(R.id.passwordEditText);
            TextView errorTextView = findViewById(R.id.ErrorTextView);

            String userName = userNameEditText.getText().toString();
            String password = passwordEditText.getText().toString();

            LoginManager loginManager = new LoginManager(userName, password);

            if(loginManager.hasValidCredentials()) {
                errorTextView.setVisibility(View.INVISIBLE);
            }else{
                errorTextView.setText(R.string.error_text);
                errorTextView.setVisibility(View.VISIBLE);
            }
        }
    };
}
