package com.example.todolist;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView todoList = findViewById(R.id.todo_list);

        List<ToDoItem> list = new ArrayList<ToDoItem>();
        list.add(new ToDoItem("Get Milk", false));
        list.add(new ToDoItem("Walk the dog", true));
        list.add(new ToDoItem("Go to the gym", false));

        ToDoItemAdapter adapter = new ToDoItemAdapter(this, list);

        todoList.setAdapter(adapter);
    }

    private class ToDoItemAdapter extends ArrayAdapter<ToDoItem>{

        private Context context;
        private List<ToDoItem> items;

        private ToDoItemAdapter(
                Context context,
                List<ToDoItem> items
        ){
            super(context, -1, items);

            this.context = context;
            this.items = items;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent){

            if(convertView == null){
                convertView = LayoutInflater.from(context).inflate(
                        R.layout.to_do_item_layout,
                        parent,
                        false
                );
            }

            TextView itemTextView = convertView.findViewById(R.id.itemTextView);
            CheckBox completedCheckBox = convertView.findViewById(R.id.completedCheckBox);

            itemTextView.setText(items.get(position).getDescription());
            completedCheckBox.setChecked(items.get(position).isComplete());
            return  convertView;
        }
    }
}
