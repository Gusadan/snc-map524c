package com.example.weatherviewer;

import androidx.appcompat.app.AppCompatActivity;

import android.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity implements OnWeatherRequestCompleted{

    public static final String WEATHER_KEY = "WEATHER_DATA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton searchButton = findViewById(R.id.searchButton);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EditText searchText = findViewById(R.id.searchText);
                String city = searchText.getText().toString();

                WeatherRequest task = new WeatherRequest(MainActivity.this);
                task.execute(city);
            }
        });
    }

    @Override
    public void onTaskCompleted(WeatherData data) {

        Bundle args = new Bundle();
        args.putParcelable(WEATHER_KEY, data);

        WeatherDetails weatherDetails = new WeatherDetails();
        weatherDetails.setArguments(args);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

        fragmentTransaction.replace(R.id.weatherFragment, weatherDetails).commit();
    }

    public static class WeatherRequest extends AsyncTask<String, Void, WeatherData>{

        private OnWeatherRequestCompleted listener;

        public WeatherRequest(
                OnWeatherRequestCompleted listener
        ){
            this.listener = listener;
        }

        @Override
        protected WeatherData doInBackground(String... params) {

            String city = params[0];

            WeatherData data = new WeatherData(
                    city,
                    20,
                    25,
                    5,
                    "Sunny"
            );

            return data;
        }

        @Override
        protected void onPostExecute(WeatherData data) {
            listener.onTaskCompleted(data);
        }
    }
}
