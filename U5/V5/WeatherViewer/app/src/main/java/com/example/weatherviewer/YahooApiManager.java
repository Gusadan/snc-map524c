package com.example.weatherviewer;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class YahooApiManager {

    private String city;

    public YahooApiManager(String city){
        this.city = city;
    }

    public WeatherData getWeather(){

        JSONObject jsonObject = makeRequest();

        WeatherData data = new WeatherData(
                city,
                20,
                25,
                5,
                "Sunny"
        );
        return data;
    }

    private JSONObject makeRequest(){

        final String appId = "";
        final String baseUrl = "https://weather-ydn-yql.media.yahoo.com/forecastrss";

        String urlString = baseUrl + "?location=" + city + "&format=json&u=c";

        HttpURLConnection urlConnection;
        JSONObject json = null;

        try {
            URL url = new URL(urlString);

            String authorizationLine = OAuthManager.GetAuthorization(baseUrl, city);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setRequestProperty("Yahoo-App-Id", appId);
            urlConnection.setRequestProperty("Authorization", authorizationLine);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.connect();

            InputStream inStream = urlConnection.getInputStream();
            BufferedReader bReader = new BufferedReader(
                    new InputStreamReader(inStream)
            );

            String temp;
            StringBuilder response = new StringBuilder();

            while((temp=bReader.readLine()) != null){
                response.append(temp);
            }

            json = (JSONObject) new JSONTokener(response.toString()).nextValue();

        }catch(IOException| JSONException e){
            Log.e("WeatherView", e.getClass().getName() + ": " + e.getMessage());
        }

        Log.e("WeatherData", json.toString());
        return json;
    }
}
